#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<conio.h>

struct node
{
    int val, val_left;
    struct node *next, *next_left;
};

struct node *head = NULL, *head_left = NULL;
struct node *curr = NULL, *curr_left = NULL;

struct node* create_list(int val)
{
    struct node *ptr = (struct node*)malloc(sizeof(struct node));
    if(NULL == ptr){
        printf("\n Node creation failed \n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;

    head = curr = ptr;
    return ptr;
}

struct node* create_list_left(int val_left)
{
    struct node *ptr_left = (struct node*)malloc(sizeof(struct node));
    if(NULL == ptr_left){
        printf("\n Node creation failed \n");
        return NULL;
    }
    ptr_left->val_left = val_left;
    ptr_left->next_left = NULL;

    head_left = curr_left = ptr_left;
    return ptr_left;
}

struct node* add_to_list_right(int val, bool add_to_list_first)
{
    if(NULL == head)
        return (create_list(val));

    struct node *ptr = (struct node*)malloc(sizeof(struct node));
    if(NULL == ptr){
        printf("\n Node creation failed \n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;

    if(add_to_list_first){
        ptr->next = head;
        head = ptr;
    }
    else{
        curr->next = ptr;
        curr = ptr;
    }
    return ptr;
}

struct node* add_to_list_left(int val_left, bool add_to_list_first)
{
    if(NULL == head_left)
        return (create_list_left(val_left));

    struct node *ptr_left = (struct node*)malloc(sizeof(struct node));
    if(NULL == ptr_left){
        printf("\n Node creation failed \n");
        return NULL;
    }
    ptr_left->val_left = val_left;
    ptr_left->next_left = NULL;

    if(add_to_list_first){
        ptr_left->next_left = head_left;
        head_left = ptr_left;
    }
    else{
        curr_left->next_left = ptr_left;
        curr_left = ptr_left;
    }
    return ptr_left;
}

struct node* search_in_list(int val, struct node **prev)
{
    struct node *ptr = head;
    struct node *tmp = NULL;
    bool found = false;

    while(ptr != NULL){
        if(ptr->val == val){
            found = true;
            break;
        }
        else{
            tmp = ptr;
            ptr = ptr->next;
        }
    }

    if(true == found){
        if(prev)
            *prev = tmp;
        return ptr;
    }
    else
        return NULL;
}

struct node* search_in_list_left(int val_left, struct node **prev_left)
{
    struct node *ptr_left = head_left;
    struct node *tmp_left = NULL;
    bool found_left = false;

    while(ptr_left != NULL){
        if(ptr_left->val_left == val_left){
            found_left = true;
            break;
        }
        else{
            tmp_left = ptr_left;
            ptr_left = ptr_left->next_left;
        }
    }

    if(true == found_left){
        if(prev_left)
            *prev_left = tmp_left;
        return ptr_left;
    }
    else
        return NULL;
}

int delete_from_list(int val)
{
    struct node *prev = NULL;
    struct node *del = NULL;

    del = search_in_list(val, &prev);

    if(del == NULL)
        return -1;

    else{
        if(prev != NULL)
            prev->next = del->next;

        else if(del == head)
            head = del->next;
    }

    free(del);
    del = NULL;

    return 0;
}

int delete_from_list_left(int val_left)
{
    struct node *prev_left = NULL;
    struct node *del_left = NULL;

    del_left = search_in_list_left(val_left, &prev_left);

    if(del_left == NULL)
        return -1;

    else{
        if(prev_left != NULL)
            prev_left->next_left = del_left->next_left;

        else if(del_left == head_left)
            head_left = del_left->next_left;
    }

    free(del_left);
    del_left = NULL;

    return 0;
}


int print_list_right()
{
    struct node *ptr = head;

    while(ptr != NULL)
    {
        printf("%d ",ptr->val);
        ptr = ptr->next;
    }

    return;
}

int print_list_left()
{
    struct node *ptr_left = head_left;

    while(ptr_left != NULL)
    {
        printf("%d ",ptr_left->val_left);
        ptr_left = ptr_left->next_left;
    }

    return;
}

int print_list_left_1()
{
    struct node *ptr_left = head_left;

    while(ptr_left != NULL)
    {
        printf("%d ",ptr_left->val_left);
        ptr_left = ptr_left->next_left;
    }

    return;
}




int display_1()
{
    printf("Priest = 1\nDevil = 2\n\n");
}

int display_2()
{
    printf("\n\n\n                Priests and Devils\n\n");
    printf("Rules:\nGet the priests and devils to the left side by crossing the \nriver, make sure that the both side has the same or larger \nnumber of priests. If the number of priests is smaller than\ndevils, the devils will kill the priests and you will lose.\n\n\n\n ");
}

int display_3()
{
    printf("\n_________________                        \\_______/__________________");
    printf("\n    Left side    -------------------------\\-----/    Right side");
    printf("\n                           River            Boat  \n");
}

int display_10(int bbb)
{
    printf("\n_________________                        \\_____%d_/__________________",bbb);
    printf("\n    Left side    -------------------------\\-----/    Right side");
    printf("\n                           River            Boat  ");
}

int display_4()
{
    printf("\n_________________\\_______/                        __________________");
    printf("\n    Left side     \\-----/-------------------------    Right side");
    printf("\n                     Boat          River              \n");
}

int display_5(int bb1, int bb2)
{
    printf("\n_________________                        \\_%d___%d_/__________________", bb1, bb2);
    printf("\n    Left side    -------------------------\\-----/    Right side");
    printf("\n                           River            Boat  \n");
    return bb2;
}

int display_6(int b_left)
{
    printf("\n_________________\\_____%d_/                        __________________", b_left);
    printf("\n    Left side     \\------/-------------------------    Right side");
    printf("\n                     Boat          River              \n");
    return b_left;

}

int display_9(int bb_left)
{
    printf("\n_________________\\_%d_____/                        __________________", bb_left);
    printf("\n    Left side     \\------/-------------------------    Right side");
    printf("\n                     Boat          River              \n");
    return bb_left;
}


int display_7(int b7)
{
    printf("\n_________________                        \\_%d_____/__________________", b7);
    printf("\n    Left side    -------------------------\\------/    Right side");
    printf("\n                           River            Boat  \n");
    return b7;

}

int display_8(int bbb1, int bbb2)
{
    printf("\n_________________\\_%d___%d_/                        __________________", bbb1, bbb2);
    printf("\n    Left side    \\------/-------------------------    Right side");
    printf("\n                     Boat          River              \n");
}

int main()
{
    int ret = 0,ret_left = 0, n, n_first, i, i_first, get_boat_left, get_boat,bb,bbb,b,b5,choice,c1,c2,c3,c4,count0,count1=0,count2=0,count3=0,count4=0,count5=0,count6=0,count7=0,count8=0,count9=0;
    int priests_right = 3, devils_right = 3, priests_left = 0, devils_left = 0;
    char ans;
    display_1();

    printf("\nEnter sum of priests and devils:  ");
    scanf("%d", &n);

    int count[n];

    for(i=0 ; i<n ; i++){
        scanf("%d", &count[n]);
        add_to_list_right(count[n], false);
    }

    display_2();

    printf("\n                                                     ");
    print_list_right();

    display_3();

    printf("\nGet in boat left press 1: ");
    do {
        scanf("%d", &choice);
        printf("\n");

        switch (choice) {

            case 0:
                printf("\n\nGet in boat:  ");
                scanf("%d", &get_boat);
                //add_to_list_left(get_boat, false);
                ret = delete_from_list_left(get_boat);
                if(ret != 0)
                    return;

                printf("    ");
                print_list_left();
                printf("                                                     ");
                print_list_right();



                b = display_9(get_boat);
                count0++;

                printf("\nCross back the river press 5: ");
                break;

            case 1:
                printf("\n\nGet in boat:  ");
                scanf("%d", &get_boat);
                //add_to_list_left(get_boat, false);
                ret = delete_from_list(get_boat);
                if(ret != 0)
                    return;


               if(count1 == 0){
                    add_to_list_left(get_boat, false);
                    printf("\n                                                     ");
                    print_list_right();
                }

                else{
                    //add_to_list_left(get_boat, false);
                    printf("    ");
                    print_list_left();
                    printf("                                                     ");
                    print_list_right();
                }


                bb = display_7(get_boat);
                count1++;

                if(get_boat == 1)
                   priests_right--;
                else
                    devils_right--;

                printf("\nGet in boat right press 2: ");
                break;

            case 2:
                printf("\n\nGet in boat:  ");
                scanf("%d", &get_boat);
                //add_to_list_left(get_boat, false);
                ret = delete_from_list(get_boat);
                if(ret != 0)
                    return;

                if(count2 == 0){
                    add_to_list_left(get_boat, false);
                    printf("\n                                                     ");
                    print_list_right();
                }

                else{
                    //add_to_list_left(get_boat, false);
                    printf("    ");
                    print_list_left();
                    printf("                                                     ");
                    print_list_right();
                }


                bbb = display_5(bb,get_boat);
                count2++;
                if(get_boat == 1)
                   priests_right--;
                else
                    devils_right--;

                printf("\n\nCross the river press 3: ");
                break;

            case 3:
                if(count3 == 0){
                    printf("\n                                                     ");
                    print_list_right();
                }
                else{
                    printf("    ");
                    print_list_left();
                    printf("                                                     ");
                    print_list_right();
                }

                display_8(bb, get_boat);


                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }
                else if(priests_right + devils_right == 0){
                        printf("\n\n\n            Well done!!!\n\n");
                        return;
                }

                count3++;
                printf("\n\nGet left side press  4: ");
                break;

            case 4:
                printf("\n\nChoice:  ");
                scanf("%d", &get_boat_left);

                if(count4 == 0){
                    if(get_boat_left != bb){
                    ret_left = delete_from_list_left(bb);

                    if(ret_left != 0){
                        return;
                    }
                    else{
                        printf("\n");
                        printf("    ");
                        print_list_left();
                        printf("                                                     ");
                        print_list_right();
                        display_9(bb);
                    printf("\n\nTo empty the boat press 9: ");
                    //printf("\nCross back the river press 5: ");
                    //printf("\nEnter your choice: ");
                    }

                    }

                    else if(get_boat_left == bb){
                        ret_left = delete_from_list_left(bbb);

                        if(ret_left != 0){
                            return;
                        }

                        else{
                            printf("\n");
                            printf("    ");
                            print_list_left();
                            printf("                                                     ");
                            print_list_right();
                            display_6(bbb);
                            printf("\n\nTo empty the boat press 9: ");
                            //printf("\nCross back the river press 6: ");
                            //printf("\nEnter your choice: ");
                        }

                    }
                }

                else{

                    if(get_boat_left != bb){
                    add_to_list_left(bbb, false);


                        printf("\n");
                        printf("    ");
                        print_list_left();
                        printf("                                                     ");
                        print_list_right();
                        display_9(bb);
                        printf("\n\nTo empty the boat press 9: ");
                        //printf("\nCross back the river press 5: ");
                        //printf("\nEnter your choice: ");
                    }


                    else if(get_boat_left == bb){
                        add_to_list_left(bb, false);
                        printf("\n");
                        printf("    ");
                        print_list_left();
                        printf("                                                     ");
                        print_list_right();
                        display_6(bbb);
                        printf("\n\nTo empty the boat press 9: ");
                        //printf("\nCross back the river press 6: ");
                        //printf("\nEnter your choice: ");
                        }
                }

                if(get_boat_left == 1)
                   priests_left++;
                else
                    devils_left++;

                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }
                else if(priests_right + devils_right == 0){
                        printf("\n\n\n            Well done!!!\n\n");
                        return;
                }

                count4++;
                break;

            case 5:
                printf("    ");
                print_list_left();
                printf("                                                     ");

                print_list_right();
                b5 = display_7(b);



                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }


                printf("\nEmpty boat press 7: ", b);
                //printf("\nGet in boat right press 1: ");
                //printf("\nEnter your choice: ");


                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }

                count5++;
                break;

            case 6:
                c2 = b5;

                if(count7 != 0){
                    if(c2 == 1)
                    add_to_list_right(c2, true);
                else
                    add_to_list_right(c2, false);

                printf("    ");
                print_list_left();
                printf("                                                     ");
                print_list_right();
                display_3();

                }

                if(c2 == 1)
                   priests_right++;
                else
                    devils_right++;

                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }
                else if(priests_right + devils_right == 0){
                        printf("\n\n\n            Well done!!!\n\n");
                        return;
                }

                printf("\nGet in boat left press 1: ");
                //printf("\nGet in boat right press 1: ");
                count7++;
                break;


            case 7:
                printf("\nEnter your choice: ");
                scanf("%d", &c3);
                if(c3 == 1)
                    add_to_list_right(c3, true);
                else
                    add_to_list_right(c3, false);

                printf("    ");
                print_list_left();
                printf("                                                     ");
                print_list_right();
                display_3();

                if(c2 == 1)
                   priests_right++;
                else
                    devils_right++;

                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }
                else if(priests_right + devils_right == 0){
                        printf("\n\n\n            Well done!!!\n\n");
                        return;
                }

                printf("\nGet in boat left press 1: ");
                //printf("\nGet in boat right press 1: ");
                count7++;
                break;


            case 8:
                printf("\nEnter your choice: ");
                scanf("%d", &c3);
                //c1= b;

                if(c3 == 2)
                    add_to_list_right(c3, false);
                else
                    add_to_list_right(c3, true);

                printf("    ");
                print_list_left();
                printf("                                                     ");
                print_list_right();
                display_3();

                if(c1 == 1)
                   priests_right++;
                else
                    devils_right++;

                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }
                else if(priests_right + devils_right == 0){
                        printf("\n\n\n            Well done!!!\n\n");
                        return;
                }

                printf("\nGet in boat left press 1: ");
                //printf("\nGet in boat right press 1: ");
                count8++;
                break;

            case 9:
                printf("\n\nChoice:  ");
                scanf("%d", &get_boat_left);
                //add_to_list_left(get_boat_left, false);

                if(count4 == 0){
                    if(get_boat_left != bb){
                    ret_left = delete_from_list_left(bb);

                    if(ret_left != 0){
                        return;
                    }
                    else{
                        printf("\n");
                        printf("    ");
                        print_list_left();
                        printf("                                                     ");
                        print_list_right();
                        display_4();
                    printf("\nGet in boat left press 0: ");

                    }

                    }

                    else if(get_boat_left == bb){
                        ret_left = delete_from_list_left(bbb);

                        if(ret_left != 0){
                            return;
                        }

                        else{
                            printf("\n");
                            printf("    ");
                            print_list_left();
                            printf("                                                     ");
                            print_list_right();
                            display_4();
                            printf("\nGet in boat left press 0: ");
                        }

                    }
                }

                else{

                    if(get_boat_left != bb){
                    add_to_list_left(get_boat_left, false);


                        printf("\n");
                        printf("    ");
                        print_list_left();
                        printf("                                                     ");
                        print_list_right();
                        display_4();
                       printf("\nGet in boat left press 1: ");
                    }
                    else (get_boat_left == bb); {
                        add_to_list_left(bb, false);
                        printf("\n");
                        printf("    ");
                        print_list_left();
                        printf("                                                     ");
                        print_list_right();
                        display_4();
                        printf("\nGet in boat left press 0: ");
                        }
                }

                if(get_boat_left == 1)
                   priests_left++;
                else
                    devils_left++;

                if(priests_right<devils_right){
                    printf("\n\n\n              Priests are dead!!!\n\n");
                    return;
                }
                else if(priests_right + devils_right == 0){
                        printf("\n\n\n            Well done!!!\n\n");
                        return;
                }

                count4++;
                count9++;
                break;


            }

        }
        while(1);

}
